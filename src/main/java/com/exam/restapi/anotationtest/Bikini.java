package com.exam.restapi.anotationtest;

import org.springframework.stereotype.Component;

@Component
public class Bikini implements Outfit {
    private String bikini;

    @Override
    public void wear() {
        System.out.println("mac bikini");
        bikini = "bikini";
    }

    public String getBikini() {
        return bikini;
    }
}
