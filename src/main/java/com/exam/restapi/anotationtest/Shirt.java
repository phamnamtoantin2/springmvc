package com.exam.restapi.anotationtest;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class Shirt implements Outfit{
    @Override
    public void wear() {
        System.out.println("mac shirt");
    }
}
