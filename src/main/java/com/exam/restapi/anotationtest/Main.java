package com.exam.restapi.anotationtest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Main {
    String message = "bikini1";

    @Test
    public void testPrintMessage() {
        System.out.println("Inside testPrintMessage()");
        Bikini outfit=new Bikini();
        outfit.wear();
        assertEquals(message, outfit.getBikini());
    }
}
