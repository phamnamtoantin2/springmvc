package com.exam.restapi.anotationtest;

public interface Outfit {
    public void wear();
}
