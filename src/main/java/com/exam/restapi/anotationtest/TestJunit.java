package com.exam.restapi.anotationtest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({Main.class})
public class TestJunit {
}
