package com.exam.restapi.controller;

import com.exam.restapi.entity.Product;
import com.exam.restapi.sercurity.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class HelloController {
    @Autowired
    IProductService productService;

    @GetMapping("/list")
    public ResponseEntity<?> getProducts() {
        List<Product> products = productService.listAll();
        return ResponseEntity.ok(products);
    }

    @PostMapping("/add")
    public ResponseEntity<?> add(@RequestBody Product product) {
        try {
            productService.addProduct(product);
        }catch (Exception e){
            return new ResponseEntity<>("body", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok("productService.addProduct(product)");
    }
}
