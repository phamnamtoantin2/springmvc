package com.exam.restapi.sercurity;

import com.exam.restapi.entity.Product;
import org.springframework.stereotype.Service;

import java.util.List;


public interface IProductService {
    public List<Product> listAll();
    public Product addProduct(Product product);
}
