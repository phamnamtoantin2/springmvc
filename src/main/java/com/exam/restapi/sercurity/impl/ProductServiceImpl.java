package com.exam.restapi.sercurity.impl;

import com.exam.restapi.entity.Product;
import com.exam.restapi.repository.ProductRepository;
import com.exam.restapi.sercurity.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements IProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> listAll() {
        return productRepository.findAll();
    }

    @Override
    public Product addProduct(Product product) {
        return productRepository.save(product);
    }
}
